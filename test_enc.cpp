//#include <stdio.h>
#include <sys/time.h>
#include "v4l2cap.hpp"
#include "omx_enc_noncam.hpp"
#include "streamer.hpp"

inline double get_dtime2(void){
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return ((double)(tv.tv_sec) + (double)(tv.tv_usec) * 0.001 * 0.001);
}


int main(){
  encoder_omx encomx;
  v4l2cap v4c;
  Streamer strm;
  int8_t * buff = NULL;
  double v1, v2;
  v1 = v2 = 0;

  strm.initialize("192.168.10.1", 12345);

  printf("Initialize camera\n");
  v4c.initialize("/dev/video0", v4l2cap::IMGSIZE_QVGA, v4l2cap::IMGFMT_YUYV, v4l2cap::FRATE_60);
  
  printf("Initialize encoder\n");
  encomx.encoder_initialize(v4l2cap::IMGSIZE_QVGA, v4l2cap::FRATE_60);
  
  printf("Inited\n");

  printf("Start capturing\n");
  v4c.startcap();
  
  while(1){
    if(v4c.readstreamb() == 1){
      buff = malloc(v4c.outimgsize);
      memcpy(buff, v4c.outimg, v4c.outimgsize);

      encomx.encoder_encode(buff);
      //65515
      //strm.send_split(encomx.out_buff, encomx.size_out, 61002);      
      strm.send_split(encomx.out_buff, encomx.size_out, 61002);
      
      v2 = v1;
      v1 = get_dtime2();

      printf("FPS:%5.5f, S:%d\n", 1.0 / (v1 - v2), v4c.outimgsize);
      
      free(buff);
    }
  }


  encomx.encoder_deinitialize();
  printf("Deinited\n");

  v4c.stopcap();
  v4c.closecam();  
  
  return 1;

}


















