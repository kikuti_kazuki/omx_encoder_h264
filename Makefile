OBJS=encode.o
BIN=hello_encode.bin

SDKSTAGE = $(HOME)/rpi
CC = $(HOME)/rpi/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin/arm-linux-gnueabihf-gcc
CXX = $(HOME)/rpi/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin/arm-linux-gnueabihf-g++


CFLAGS+=-DSTANDALONE -D__STDC_CONSTANT_MACROS -D__STDC_LIMIT_MACROS -DTARGET_POSIX -D_LINUX -fPIC -DPIC -D_REENTRANT -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -U_FORTIFY_SOURCE -Wall -g -DHAVE_LIBOPENMAX=2 -DOMX -DOMX_SKIP64BIT -ftree-vectorize -pipe -DUSE_EXTERNAL_OMX -DHAVE_LIBBCM_HOST -DUSE_EXTERNAL_LIBBCM_HOST -DUSE_VCHIQ_ARM -Wno-psabi -fpermissive -mcpu=cortex-a7 -mfpu=neon -mfloat-abi=hard -O3

CXXFLAGS+=$(CFLAGS)

LDFLAGS+=-L$(SDKSTAGE)/vc/src/hello_pi/libs/ilclient -L$(SDKSTAGE)/vc/src/hello_pi/libs/vgfont -L$(SDKSTAGE)/vc/lib/ -lilclient -lGLESv2 -lEGL -lopenmaxil -lbcm_host -lvcos -lvchiq_arm -lpthread -lrt -lm

INCLUDES+=-I$(SDKSTAGE)/vc/include/ -I$(SDKSTAGE)/vc/include/interface/vcos/pthreads -I$(SDKSTAGE)/vc/include/interface/vmcs_host/linux -I$(SDKSTAGE)/vc/src/hello_pi/ -I$(SDKSTAGE)/vc/src/hello_pi/libs/ilclient -I$(SDKSTAGE)/vc/src/hello_pi/libs/vgfont

TARGET = test_enc.bin
SRCS = test_enc omx_enc_noncam v4l2cap streamer
OBJS = $(addsuffix .o,$(SRCS))

#リンクの依存関係か今のところわからない　そのため、Whole-archiveが必要になる
$(TARGET):  $(OBJS)
	$(CXX) -o $@ -Wl,--whole-archive $(LDFLAGS) $^ -Wl,--no-whole-archive -rdynamic
#-Wl,--no-whole-archive
.cpp.o:
	$(CXX) $(CXXFLAGS) $(INCLUDES) -g -c $< -Wno-deprecated-declarations

.PHONY: clean
clean:
	$(RM) $(TARGET) $(OBJS)

.PHONY: deploy
deploy:
	./deploy $(TARGET)

