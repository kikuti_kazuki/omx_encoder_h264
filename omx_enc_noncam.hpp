//Cam captured image encoding using OpenMAX IL
//through the ilclient helper library


#ifndef ENC_OMX_NONCAM_H
#define ENC_OMX_NONCAM_H

#include <sys/types.h>

#include "bcm_host.h"
#include "ilclient.h"


//Class for h.264 encoder using OpenMAX IL with IL helper API
//Encode flow
/*
  Initialize IL client
  Initialize OMX

  Create component: video_encode
  Set video_encode to parameters
    Image size
    Frame rate
    Color format
    Compression format
    Bit rate
    Port index

  Set video_encoder status to idle
  
  Enable port buffers, 200, 201

  Set video_encoder status to executing

  Initialize Camera by the parameters of video_encode

  #Encodig loop

  Get a pointer to input buffer of encoder
  Write captured image to the input buffer
  Request encoder to empty the input buffer

  Get a pointer to output coded buffer of encoder 
  Request encoder to fill the input buffer

  Output data or send data to mother computer

  #Encoding loop
  
  Post Processing...


*/
class encoder_omx{
public:
  encoder_omx();
  ~encoder_omx();

  int8_t encoder_initialize(const int imgsize, const int frate);
  int8_t encoder_encode(const int8_t *src);
  int8_t encoder_deinitialize();

  //Output iamge, size
  char * out_buff;
  int size_out;
  
private:

  int size_image;
  int height_image;
  int width_image;
  int frate_image;
  int format_image;

  int bitrate_image;


  OMX_VIDEO_PARAM_PORTFORMATTYPE format;
  OMX_PARAM_PORTDEFINITIONTYPE def;
  COMPONENT_T *video_encode;
  COMPONENT_T *list[5];
  OMX_BUFFERHEADERTYPE *buf;
  OMX_BUFFERHEADERTYPE *out;
  OMX_ERRORTYPE r;
  ILCLIENT_T *client;

  
  int8_t convertYUV4222YUV420P(char * dst, char * src, int width, int height);
  void convert_yuv422_to_yuv420(char *InBuff, char *OutBuff, int width, int height);
  //v4l2cap::IMGFMT_YUYV
};



#endif //ENC_OMX_NONCAM_H
