//Cam captured image encoding using OpenMAX IL
//through the ilclient helper library

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>


#include "bcm_host.h"
#include "ilclient_cpp.h"

#include <IL/OMX_Core.h>
#include <IL/OMX_Component.h>
#include <IL/OMX_Video.h>
#include <IL/OMX_Broadcom.h>

#include "v4l2cap.hpp"
#include "omx_enc_noncam.hpp"


/*
  今の問題
  動かしていると遅延が生じてくる
  リセットするともとに戻る
*/



encoder_omx::encoder_omx()
//Initialize variables
{
  size_image = 0;
  height_image = 0;
  width_image = 0;
  frate_image = 0;
  format_image = 0;

  bitrate_image = 0;

  out_buff = NULL;
  size_out = 0;

}

encoder_omx::~encoder_omx(){

}

int8_t encoder_omx::encoder_initialize(const int imgsize, const int frate){

  //Initialize camera
  /*
    Init cam
    Start cap
    Read streamb
    Stop cap
    Close cam
   */
  // v4c.initialize("/dev/video0", imgsize, v4l2cap::IMGFMT_YUYV, frate);
  width_image = v4l2cap::IMGSIZE_SET[imgsize][0];
  height_image = v4l2cap::IMGSIZE_SET[imgsize][1];
  format_image = v4l2cap::IMGFMT_SET[v4l2cap::IMGFMT_YUYV];
  frate_image = v4l2cap::FRATE_SET[frate];

  bcm_host_init();
  
  memset(list, 0, sizeof(list));
  
  //initialize IL client
  if((client = ilclient_init()) == NULL){
    return -3;
  }

  // OMX初期化
  if (OMX_Init() != OMX_ErrorNone) {
    ilclient_destroy(client);
    return -4;
   }

  
  //エンコーダコンポーネントの生成
  // create video_encode
  r = ilclient_create_component(client, &video_encode, "video_encode",
				ILCLIENT_DISABLE_ALL_PORTS |
				ILCLIENT_ENABLE_INPUT_BUFFERS |
				ILCLIENT_ENABLE_OUTPUT_BUFFERS);
  if (r != 0) {
      printf
	("ilclient_create_component() for video_encode failed with %x!\n",
	 r);
      exit(1);
  }
  list[0] = video_encode;
  
  //video_encodeコンポーネントの現在設定を取得
  // get current settings of video_encode component from port 200
  memset(&def, 0, sizeof(OMX_PARAM_PORTDEFINITIONTYPE));
  def.nSize = sizeof(OMX_PARAM_PORTDEFINITIONTYPE);
  def.nVersion.nVersion = OMX_VERSION;
  def.nPortIndex = 200;

  //Get param
  if (OMX_GetParameter
      (ILC_GET_HANDLE(video_encode), OMX_IndexParamPortDefinition,
       &def) != OMX_ErrorNone) {
    printf("%s:%d: OMX_GetParameter() for video_encode port 200 failed!\n",
	   __FUNCTION__, __LINE__);
    exit(1);
  }
  
  //エンコーダを低遅延モードに設定 クラッシュして再起動も不可となる
  //設定場所により変わる　ここは良い　おそらくエンコーダ設定の前に行うと良い
  //↑やっぱダメでした
  OMX_CONFIG_BOOLEANTYPE lowlatency;
  memset(&lowlatency, 0, sizeof(OMX_CONFIG_BOOLEANTYPE));
  lowlatency.nSize = sizeof(OMX_CONFIG_BOOLEANTYPE);
  lowlatency.nVersion.nVersion = OMX_VERSION;
  lowlatency.bEnabled = OMX_FALSE;
  r = OMX_SetParameter(ILC_GET_HANDLE(video_encode),
                       OMX_IndexConfigBrcmVideoH264LowLatency, &lowlatency);
  if (r != OMX_ErrorNone) {
    printf
      ("%s:%d: OMX_SetParameter() for lowlatency mode failed with: %x!\n",
       __FUNCTION__, __LINE__, r);
    exit(1);
  }
 
  

  //初期パラメータの初期化
  //print_def(def);
  
  //今回使用する設定をエンコーダに適用する
  // Port 200: in 1/1 115200 16 enabled,not pop.,not cont. 320x240 320x240 @1966080 20
  def.format.video.nFrameWidth = width_image;
  def.format.video.nFrameHeight = height_image;
  def.format.video.xFramerate = frate_image << 16;
  def.format.video.nSliceHeight = def.format.video.nFrameHeight;
  def.format.video.nStride = def.format.video.nFrameWidth;
  def.format.video.eColorFormat = OMX_COLOR_FormatYUV420PackedPlanar;
  
  //print_def(def);
  
  //setting parameters
  r = OMX_SetParameter(ILC_GET_HANDLE(video_encode),
		       OMX_IndexParamPortDefinition, &def);
  if (r != OMX_ErrorNone) {
    printf
      ("%s:%d: OMX_SetParameter() for video_encode port 200 failed with %x!\n",
       __FUNCTION__, __LINE__, r);
    exit(1);
  }

   //エンコーディング形式の設定
  memset(&format, 0, sizeof(OMX_VIDEO_PARAM_PORTFORMATTYPE));
  format.nSize = sizeof(OMX_VIDEO_PARAM_PORTFORMATTYPE);
  format.nVersion.nVersion = OMX_VERSION;
  format.nPortIndex = 201;
  format.eCompressionFormat = OMX_VIDEO_CodingAVC;

  printf("OMX_SetParameter for video_encode:201...\n");
  r = OMX_SetParameter(ILC_GET_HANDLE(video_encode),
		       OMX_IndexParamVideoPortFormat, &format);
  if (r != OMX_ErrorNone) {
    printf
      ("%s:%d: OMX_SetParameter() for video_encode port 201 failed with %x!\n",
       __FUNCTION__, __LINE__, r);
    exit(1);
  }

  //ビットレートの設定　１Mbitのビットレートに設定している
  OMX_VIDEO_PARAM_BITRATETYPE bitrateType;
  // set current bitrate to 1Mbit
  memset(&bitrateType, 0, sizeof(OMX_VIDEO_PARAM_BITRATETYPE));
  bitrateType.nSize = sizeof(OMX_VIDEO_PARAM_BITRATETYPE);
  bitrateType.nVersion.nVersion = OMX_VERSION;
  bitrateType.eControlRate = OMX_Video_ControlRateVariable;
  bitrateType.nTargetBitrate = 20000000;
  bitrateType.nPortIndex = 201;
  r = OMX_SetParameter(ILC_GET_HANDLE(video_encode),
                       OMX_IndexParamVideoBitrate, &bitrateType);
  if (r != OMX_ErrorNone) {
    printf
      ("%s:%d: OMX_SetParameter() for bitrate for video_encode port 201 failed with %x!\n",
       __FUNCTION__, __LINE__, r);
    exit(1);
  }
  
  //現在のビットレートを取得し、表示
  // get current bitrate
  memset(&bitrateType, 0, sizeof(OMX_VIDEO_PARAM_BITRATETYPE));
  bitrateType.nSize = sizeof(OMX_VIDEO_PARAM_BITRATETYPE);
  bitrateType.nVersion.nVersion = OMX_VERSION;
  bitrateType.nPortIndex = 201;

  if (OMX_GetParameter
      (ILC_GET_HANDLE(video_encode), OMX_IndexParamVideoBitrate,
       &bitrateType) != OMX_ErrorNone) {
    printf("%s:%d: OMX_GetParameter() for video_encode for bitrate port 201 failed!\n",
	   __FUNCTION__, __LINE__);
    exit(1);
  }
  printf("Current Bitrate=%u\n",bitrateType.nTargetBitrate);


  //Enable low delay hrd flag
  OMX_CONFIG_PORTBOOLEANTYPE low_delay_hrd_flag;
  memset(&low_delay_hrd_flag, 0, sizeof(OMX_CONFIG_PORTBOOLEANTYPE));
  low_delay_hrd_flag.nSize = sizeof(OMX_CONFIG_PORTBOOLEANTYPE);
  low_delay_hrd_flag.nVersion.nVersion = OMX_VERSION;
  low_delay_hrd_flag.nPortIndex = 201;
  low_delay_hrd_flag.bEnabled = OMX_FALSE;
  r = OMX_SetParameter(ILC_GET_HANDLE(video_encode),
                       OMX_IndexParamBrcmVideoAVC_LowDelayHRDEnable,
  		       &low_delay_hrd_flag);
  if (r != OMX_ErrorNone) {
    printf
      ("%s:%d: OMX_SetParameter() set low delay hrd flag failed with: %x!\n",
       __FUNCTION__, __LINE__, r);
    exit(1);
  }

  //Enable SPS/PPS insertion
  OMX_CONFIG_PORTBOOLEANTYPE sps_pps_insertion;
  memset(&sps_pps_insertion, 0, sizeof(OMX_CONFIG_PORTBOOLEANTYPE));
  sps_pps_insertion.nSize = sizeof(OMX_CONFIG_PORTBOOLEANTYPE);
  sps_pps_insertion.nVersion.nVersion = OMX_VERSION;
  sps_pps_insertion.nPortIndex = 201;
  sps_pps_insertion.bEnabled = OMX_FALSE;
  r = OMX_SetParameter(ILC_GET_HANDLE(video_encode),
                       OMX_IndexParamBrcmVideoAVCInlineHeaderEnable,
  		       &sps_pps_insertion);
  if (r != OMX_ErrorNone) {
    printf
      ("%s:%d: OMX_SetParameter() set low in line header flag failed with: %x!\n",
       __FUNCTION__, __LINE__, r);
    exit(1);
  }

  //Enable VCL HRD parameters insertion
  OMX_CONFIG_PORTBOOLEANTYPE vcl_hrd_insertion;
  memset(&vcl_hrd_insertion, 0, sizeof(OMX_CONFIG_PORTBOOLEANTYPE));
  vcl_hrd_insertion.nSize = sizeof(OMX_CONFIG_PORTBOOLEANTYPE);
  vcl_hrd_insertion.nVersion.nVersion = OMX_VERSION;
  vcl_hrd_insertion.nPortIndex = 201;
  vcl_hrd_insertion.bEnabled = OMX_FALSE;
  r = OMX_SetParameter(ILC_GET_HANDLE(video_encode),
                       OMX_IndexParamBrcmVideoAVC_VCLHRDEnable,
  		       &vcl_hrd_insertion);
  if (r != OMX_ErrorNone) {
    printf
      ("%s:%d: OMX_SetParameter() set VCL_HRD_ENABLE flag failed with: %x!\n",
       __FUNCTION__, __LINE__, r);
    exit(1);
  }
  
 

  
  // //エンコーダを低遅延モードに設定 クラッシュして再起動も不可となる
  // OMX_CONFIG_BOOLEANTYPE lowlatency;
  // memset(&lowlatency, 0, sizeof(OMX_CONFIG_BOOLEANTYPE));
  // lowlatency.nSize = sizeof(OMX_CONFIG_BOOLEANTYPE);
  // lowlatency.nVersion.nVersion = OMX_VERSION;
  // lowlatency.bEnabled = OMX_FALSE;
  // r = OMX_SetParameter(ILC_GET_HANDLE(video_encode),
  //                      OMX_IndexConfigBrcmVideoH264LowLatency, &lowlatency);
  // if (r != OMX_ErrorNone) {
  //   printf
  //     ("%s:%d: OMX_SetParameter() for lowlatency mode failed with: %x!\n",
  //      __FUNCTION__, __LINE__, r);
  //   exit(1);
  // }


  //Set LowDelayHRDFlag
  printf("Set LowDelayHRDFlag...\n");

  
  
  //エンコーダの状態をアイドルにする
  printf("encode to idle...\n");
  if (ilclient_change_component_state(video_encode, OMX_StateIdle) == -1) {
    printf
      ("%s:%d: ilclient_change_component_state(video_encode, OMX_StateIdle) failed",
       __FUNCTION__, __LINE__);
  }
  
  //各ポートのバッファを有効にする
  printf("enabling port buffers for 200...\n");
  if (ilclient_enable_port_buffers(video_encode, 200, NULL, NULL, NULL) != 0) {
    printf("enabling port buffers for 200 failed!\n");
    exit(1);
  }
  
  printf("enabling port buffers for 201...\n");
   if (ilclient_enable_port_buffers(video_encode, 201, NULL, NULL, NULL) != 0) {
     printf("enabling port buffers for 201 failed!\n");
     exit(1);
   }
   
   //エンコーダの状態をエクスキュートにする
   printf("encode to executing...\n");
   ilclient_change_component_state(video_encode, OMX_StateExecuting);

   //出力バッファのメモリ確保
   out_buff = (char *)malloc(height_image * width_image * 3);
   size_out = -1;
   
   return 1;
}

//エンコードの実行　必ず一定時間ごとに呼ぶ必要がある
//SRCから入力されたバッファをエンコードする
//SRCはYUYVの形式
int8_t encoder_omx::encoder_encode(const int8_t *src){

  buf = ilclient_get_input_buffer(video_encode, 200, 1);
  if(buf == NULL){
    //入力バッファのポインタ取得に失敗した場合
    printf("Doh, no buffers for me!\n");
  }else{

    //YUYVからYUV420Pに変換する　将来的にはNEONで実装？
    //convertYUV4222YUV420P(buf->pBuffer, src, width_image, height_image);
    convert_yuv422_to_yuv420(src, buf->pBuffer, width_image, height_image);

    buf->nFilledLen = width_image * height_image * 3 / 2;

    //エンコーダ、ポート？にバッファを読み込んでもらう
    if (OMX_EmptyThisBuffer(ILC_GET_HANDLE(video_encode), buf) !=
	OMX_ErrorNone) {
      printf("Error emptying buffer!\n");
    }
    
    //エンコーダの出力バッファのポインタを取得
    out = ilclient_get_output_buffer(video_encode, 201, 1);

    //エンコーダに出力バッファへエンコードされたデータを書き込んでもらう
    r = OMX_FillThisBuffer(ILC_GET_HANDLE(video_encode), out);
    if (r != OMX_ErrorNone) {
      printf("Error filling buffer: %x\n", r);
    }

    //エンコードされた場合は出力バッファにエンコードされたデータをコピーする
    if(out != NULL){
      //エンコードが終わってない？場合
      if (out->nFlags & OMX_BUFFERFLAG_CODECCONFIG) {
	int i;
	for (i = 0; i < out->nFilledLen; i++)
	  printf("%x ", out->pBuffer[i]);
	printf("\n");
      }
      
      //r = fwrite(out->pBuffer, 1, out->nFilledLen, outf);
      //出力バッファにコピー
      memcpy(out_buff, out->pBuffer, out->nFilledLen);
      size_out = out->nFilledLen;
      //      printf("Outcome copied %d\n", size_out);

      out->nFilledLen = 0;

    }else{
      printf("Not getting it\n");
    }

  }

  return 1;
}

//あと処置くん
int8_t encoder_omx::encoder_deinitialize(){
  printf("Teardown.\n");

  //後処理
  printf("disabling port buffers for 200 and 201...\n");
  ilclient_disable_port_buffers(video_encode, 200, NULL, NULL, NULL);
  ilclient_disable_port_buffers(video_encode, 201, NULL, NULL, NULL);
   
  ilclient_state_transition(list, OMX_StateIdle);
  ilclient_state_transition(list, OMX_StateLoaded);
  
  ilclient_cleanup_components(list);

  OMX_Deinit();

  ilclient_destroy(client);

  free(out_buff);
  
  return 1;
}



int8_t encoder_omx::convertYUV4222YUV420P(char * dst, char * src,
					  int width, int height){
  int w, i, j, jm6, jm5, jm4, jm3, jm2, jm1;
  int jp1, jp2, jp3, jp4, jp5, jp6;
  
  w = width>>1;
  
  /* intra frame */
  for (i=0; i<w; i++)
    {
      for (j=0; j<height; j+=2)
	{
	  jm5 = (j<5) ? 0 : j-5;
	  jm4 = (j<4) ? 0 : j-4;
	  jm3 = (j<3) ? 0 : j-3;
	  jm2 = (j<2) ? 0 : j-2;
	  jm1 = (j<1) ? 0 : j-1;
	  jp1 = (j<height-1) ? j+1 : height-1;
	  jp2 = (j<height-2) ? j+2 : height-1;
	  jp3 = (j<height-3) ? j+3 : height-1;
	  jp4 = (j<height-4) ? j+4 : height-1;
	  jp5 = (j<height-5) ? j+5 : height-1;
	  jp6 = (j<height-5) ? j+6 : height-1;

	  /* FIR filter with 0.5 sample interval phase shift */
	  dst[w*(j>>1)] = src[(int)(228*(src[w*j]+src[w*jp1])
				    +70*(src[w*jm1]+src[w*jp2])
				    -37*(src[w*jm2]+src[w*jp3])
				    -21*(src[w*jm3]+src[w*jp4])
				    +11*(src[w*jm4]+src[w*jp5])
				    + 5*(src[w*jm5]+src[w*jp6])+256)>>9];
	}
      src++;
      dst++;
    }
  
}

void encoder_omx::convert_yuv422_to_yuv420(char *InBuff, char *OutBuff, int width, int height)
{
    int i = 0, j = 0, k = 0;
    int UOffset = width * height;
    int VOffset = (width * height) * 5/4;
    int UVSize = (width * height) / 4;
    int line1 = 0, line2 = 0;
    int m = 0, n = 0;
    int y = 0, u = 0, v = 0;

    u = UOffset;
    v = VOffset;

    for (i = 0, j = 1; i < height; i += 2, j += 2)
    {
        /* Input Buffer Pointer Indexes */
        line1 = i * width * 2;
        line2 = j * width * 2;

        /* Output Buffer Pointer Indexes */
        m = width * y;
        y = y + 1;
        n = width * y;
        y = y + 1;

        /* Scan two lines at a time */
        for (k = 0; k < width; k += 2)
        {
            unsigned char Y1, Y2, U, V;
            unsigned char Y3, Y4, U2, V2;

            /* Read Input Buffer */
            Y1 = InBuff[line1++];
            U  = InBuff[line1++];
            Y2 = InBuff[line1++];
            V  = InBuff[line1++];

            Y3 = InBuff[line2++];
            U2 = InBuff[line2++];
            Y4 = InBuff[line2++];
            V2 = InBuff[line2++];

            /* Write Output Buffer */
            OutBuff[m++] = Y1;
            OutBuff[m++] = Y2;

            OutBuff[n++] = Y3;
            OutBuff[n++] = Y4;

            OutBuff[u++] = U;//(U + U2)/2;
	    OutBuff[v++] = V;//(V + V2)/2;
        }
    }
}



